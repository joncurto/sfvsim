from tests.util import SimulationRun


def test_ryu_fb_h():
    run = SimulationRun("2.021", "RYU", "RYU", "ryu-fb-h", 45)
    run.execute("STAND", 9)
    run.execute_and_check("FB_H")


def test_ryu_fb_h_far():
    run = SimulationRun("2.021", "RYU", "RYU", "ryu-fb-h-far")
    run.execute("STAND", 2)
    run.execute_and_check("FB_H")


def test_ryu_fb_l_far():
    run = SimulationRun("2.021", "RYU", "RYU", "ryu-fb-l-far")
    run.execute("STAND", 2)
    run.execute_and_check("FB_L")


def test_ryu_dash_fb_l_aa_hit():
    run = SimulationRun("2.021", "RYU", "RYU", "ryu-dash-fb-l-aa-hit")
    run.execute("FORWARD", 1, move_name_p2="JUMP_V")
    run.execute("STAND", 1)
    run.execute("DASH", 17)
    run.execute_and_check("FB_L")


def test_ryu_dash_fb_l_aa_whiff():
    run = SimulationRun("2.021", "RYU", "RYU", "ryu-dash-fb-l-aa-whiff")
    run.execute("FORWARD", 1)
    run.execute("STAND", 1)
    run.execute("DASH", 17, move_name_p2="JUMP_V")
    run.execute_and_check("FB_L")


def test_ryu_fb_l_aa_far():
    run = SimulationRun("2.021", "RYU", "RYU", "ryu-fb-l-aa-far")
    run.execute("STAND", 2, move_name_p2="JUMP_V")
    run.execute_and_check("FB_L")


def test_ryu_fb_ex_far():
    run = SimulationRun("2.021", "RYU", "RYU", "ryu-fb-ex-far")
    run.execute("STAND", 2)
    run.execute_and_check("FB_EX")


def test_ryu_fb_ex_aa_far():
    run = SimulationRun("2.021", "RYU", "RYU", "ryu-fb-ex-aa-far")
    run.execute("CROUCH", 11, move_name_p2="JUMP_V")
    run.execute_and_check("FB_EX")


def test_ryu_vskill_parry():
    run = SimulationRun("2.021", "RYU", "CMY", "ryu-vskill-parry", 40)
    run.execute_and_check("V_SKILL", script_p2="5MP")


def test_ryu_shoryu_h_whiff():
    run = SimulationRun("2.021", "RYU", "CMY", "ryu-shoryu-h-whiff")
    run.execute("STAND", 2)
    run.execute_and_check("SHOURYU_H")


def test_ryu_air_tatsu_ex_whiff():
    run = SimulationRun("2.021", "RYU", "RYU", "ryu-air-tatsu-ex-whiff")
    run.execute("JUMP_F", 32)
    run.execute_and_check("AIR_TATSUMAKI_EX")
