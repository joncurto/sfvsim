from tests.util import SimulationRun


def test_z20_vskill_parry():
    run = SimulationRun("2.021", "Z20", "CMY", "z20-vskill-parry", 35)
    run.execute_and_check("V_SKILL", script_p2="2HP")
