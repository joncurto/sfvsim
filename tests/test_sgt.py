from tests.util import SimulationRun


def test_5hk_1hit():
    run = SimulationRun("3.060", "Z31", "RYU", "sgt-5hk-1hit", forward_p1=40)
    run.execute_and_check("5HK")


def test_5hk_1hit_block():
    run = SimulationRun("3.060", "Z31", "RYU", "sgt-5hk-1hit-block", forward_p1=40)
    run.p2.hit_effect = "GUARD"
    run.execute_and_check("5HK")


def test_5hk_1hit_cc():
    run = SimulationRun("3.060", "Z31", "RYU", "sgt-5hk-1hit-cc", forward_p1=40)
    run.p2.hit_effect = "COUNTERHIT"
    run.execute_and_check("5HK")


def test_5hk_2hit():
    run = SimulationRun("3.060", "Z31", "RYU", "sgt-5hk-2hit", forward_p1=50)
    run.execute_and_check("5HK")


def test_5hk_2hit_block():
    run = SimulationRun("3.060", "Z31", "RYU", "sgt-5hk-2hit-block", forward_p1=50)
    run.p2.hit_effect = "GUARD"
    run.execute_and_check("5HK")


def test_5hk_2hit_counter():
    run = SimulationRun("3.060", "Z31", "RYU", "sgt-5hk-2hit-counter", forward_p1=50)
    run.p2.hit_effect = "COUNTERHIT"
    run.execute_and_check("5HK")


def test_5hk_whiff():
    run = SimulationRun("3.060", "Z31", "RYU", "sgt-5hk-whiff")
    run.execute_and_check("5HK")
