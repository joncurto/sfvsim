from tests.util import SimulationRun


def test_ncl_5mk():
    run = SimulationRun("2.021", "NCL", "RYU", "ncl-5mk", 50)
    run.execute_and_check("5MK")


def test_ncl_5mk_guard():
    run = SimulationRun("2.021", "NCL", "RYU", "ncl-5mk-guard", 50)
    run.p2.hit_effect = "GUARD"
    run.execute_and_check("5MK")


def test_ncl_5hp_cc():
    run = SimulationRun("2.021", "NCL", "RYU", "ncl-5hp-cc", 40)
    run.p2.hit_effect = "COUNTERHIT"
    run.execute_and_check("5HP")


def test_ncl_5hk_cc():
    run = SimulationRun("2.021", "NCL", "RYU", "ncl-5hk-cc", 40)
    run.p2.hit_effect = "COUNTERHIT"
    run.execute_and_check("5HK")


def test_ncl_gaia_l():
    run = SimulationRun("2.021", "NCL", "RYU", "ncl-gaia-lk", 40)
    run.execute("STAND", 1)
    run.execute_and_check("GAIA_L")


def test_ncl_gaia_h():
    run = SimulationRun("2.021", "NCL", "RYU", "ncl-gaia-hk", 45)
    run.execute("STAND", 1)
    run.execute_and_check("GAIA_H")


def test_ncl_skill():
    run = SimulationRun("2.021", "NCL", "RYU", "ncl-vskill")
    run.execute_and_check("V_SKILL_M")


def test_ncl_vt_5mp():
    run = SimulationRun("2.021", "NCL", "RYU", "ncl-vt-5mp")
    run.p1.vtrigger = -1
    run.p1.stance = 1
    run.execute("FORWARD", 40)
    run.execute_and_check("5MP")


def test_ncl_vt_dash_5mp():
    run = SimulationRun("2.021", "NCL", "RYU", "ncl-vt-dash-5mp")
    run.p1.vtrigger = -1
    run.p1.stance = 1
    run.execute("FORWARD", 1)
    run.execute("STAND", 1)
    run.execute_and_check(["DASH", "5MP"])
