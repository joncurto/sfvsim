from sfvfile import Script, Hurtbox
from typing import TypeVar
import numpy as np

FLT = np.float32
ZERO = FLT(0)
TWO = FLT(2)

T = TypeVar('T')


def build_copy(instance, clazz):
    """
    Make a copy of the object as fast as possible (not deep, only shallow)

    :param instance: the object instance
    :param clazz: the target class
    :return: a new instance on which all attributes are the same as the input instance
    """
    new_instance = object.__new__(clazz)
    new_instance.__dict__ = instance.__dict__.copy()
    return new_instance


# noinspection PyPep8Naming
class cached_property(object):
    """
    Source: https://github.com/pydanny/cached-property/blob/master/cached_property.py
    """

    def __init__(self, func):
        self.__doc__ = getattr(func, '__doc__')
        self.func = func

    def __get__(self, obj, cls):
        if obj is None:
            return self
        value = obj.__dict__[self.func.__name__] = self.func(obj)
        return value


class PositionShiftsConstants:
    X = 0x08000
    Y = 0x10000
    RELATIVE = 0x40000
    BOUND = 0x8
    HITBOX_SHIFT = 0x4


class StatusFlag:
    CROUCHED = 0x2
    AIR = 0x4
    BOUND = 0x8
    COUNTER = 0x20
    DONT_PUSH = 0x200
    ON_GROUND = 0x8000
    AUTO_CORRECT = 0x10000
    REVERSE_AUTO_CORRECT = 0x20000


class HitEffectSituation:
    HIT = 0
    GUARD = 1
    COUNTERHIT = 2
    UNKNOWN = 3


class HitEffectPosition:
    STAND = 0
    CROUCH = 1
    AIR = 2
    OTG = 3
    UNKNOWN = 3


class HitEffectFlags:
    SPECIFIC_SCRIPT = 1
    DONT_KILL = 2
    HARD_KD = 8
    CRUSH = 0x20
    WHITE_DAMAGE = 0x40
    NO_BACK_RECOVERY = 0x4000


class HitboxFlags:
    ARMOR_BREAK = 0x2
    NO_BACK_RECOVERY = 0x10
    COMBO_THROW = 0x100
    IGNORE = 0x200
    MULTI_HIT_COUNTER = 0x200


def is_bound(flag):
    return flag == 0x30004


def is_falling(flag):
    return flag & 3 == 2 and flag > 2 or flag == 0x40004


def is_landing_required(flag):
    return flag & 0x20000


def is_knockdown(flag):
    return flag & 0x40000


def is_on_ground(flag):
    return flag & 2 == 0


def get_script_length(script: Script):
    return script.total_ticks if script.interrupt_frame < 0 else min(script.interrupt_frame + 1, script.total_ticks)


def get_collision_priority(hurt: Hurtbox):
    return -(hurt.armor_effect > 0), hurt.hurt_type != 0, -hurt.hurt_type